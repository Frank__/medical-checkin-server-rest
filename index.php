<?php
/*
** index.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: <+DESC+>
*/

require_once 'libs/Flintstone/autoload.php';
require_once 'libs/Slim/Slim.php';

require_once 'configuration.php';

require_once 'models.php';
require_once 'controllers.php';

$app = new \Slim\Slim(array(
	'debug' => true
));

require_once('./routes.php');

$app->response->headers->set('Content-Type', 'application/json');
$app->run();


/* vim: set ts=4 sw=4 noet: */

