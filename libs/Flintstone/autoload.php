<?php
/*
** autoload.php
**
*/

require_once 'Flintstone.php';
require_once 'FlintstoneDB.php';
require_once 'FlintstoneException.php';
require_once 'Formatter/FormatterInterface.php';
require_once 'Formatter/JsonFormatter.php';
require_once 'Formatter/SerializeFormatter.php';


/* vim: set ts=4 sw=4 noet: */

