<?php

/**
 * Flintstone - A key/value database store using flat files for PHP
 * Copyright (c) 2014 Jason M
 */

namespace Flintstone;

/**
 * The Flintstone database loader
 */
class Flintstone {

    /**
     * Flintstone version
     * @var string
     */
    const VERSION = '1.9';

    /**
     * Static instance
     * @var array
     */
    private static $instance = array();

	private static $options = array();

	public static function set_options(array $options) {
		self::$options = $options;
	}


    /**
     * Load a database
     *
     * @param string $database the database name
     * @param array  $options  an array of options
     *
     * @return \Flintstone\FlintstoneDB class
     *
     * @throws \Flintstone\FlintstoneException when database cannot be loaded
     */
    public static function load($database) {
        if (!array_key_exists($database, self::$instance)) {
            self::$instance[$database] = new FlintstoneDB($database, self::$options);
        }

        return self::$instance[$database];
    }

    /**
     * Unload a database
     *
     * @param string $database the database name
     */
    public static function unload($database) {
        unset(self::$instance[$database]);
    }

}
