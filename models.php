<?php
/*
** classes.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: The classes of the app
*/


class Form {
	public $id;
	public $person;
	public $address;
	public $contact;
	public $job;
	public $insurance;
	public $emergency;
	public $medical;

	function __construct($id, $form) {
		$this->id = $id;

		$this->person = $form->person;
		$this->address = $form->address;
		$this->contact = $form->contact;
		$this->job = $form->job;
		$this->insurance = $form->insurance;
		$this->emergency = $form->emergency;
		$this->medical = $form->medical;
	}
}

/* vim: set ts=4 sw=4 noet: */

