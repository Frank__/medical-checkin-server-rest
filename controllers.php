<?php
/*
** controllers.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: The controllers for the app
*/

class FormController {

	public static function create($form) {
		$forms_db = Flintstone\Flintstone::load('forms');

		//First 4 bytes are years and co; not usefull. XXX Not portable
		$generated_id = substr(uniqid(), 4); //FIXME with better id.

		$Form = new Form($generated_id, $form);


		if(strlen(serialize($Form)) > MEDICAL_MAX_OBJ_LEN) {
			exit(0);
		}

		$forms_db->set($Form->id, $Form);

		return $Form;
	}

	public static function get_by_id($id) {
		$forms = Flintstone\Flintstone::load('forms');
		$form = $forms->get($id);

		if(!$form) {
			throw new Exception('Unknown id');
		}

		return $form;
	}


}

/* vim: set ts=4 sw=4 noet: */

