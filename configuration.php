<?php
/*
** configuration.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Configuration for Slim, ActiveRecord, …
*/


//We are in Paris
date_default_timezone_set('Europe/Paris');

define('MEDICAL_MAX_OBJ_LEN', 10000);


//SLIM
\Slim\Slim::registerAutoloader();

\Flintstone\Flintstone::set_options(array(
	'dir' => 'DB',
	'formatter' => new Flintstone\Formatter\JsonFormatter()
));


/* vim: set ts=4 sw=4 noet: */

