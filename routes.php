<?php
/*
** routes.php
**
** Author:      Frank Villaro-Dixon <Frank@Villaro-Dixon.eu>
** Description: Routes for the app
*/


## /form is delegated to us
$app->group('/form', function () use ($app) {

	##Create a new form
	$app->post('', function() use ($app) {
		$input = $app->request->getBodyObject();

		$created = FormController::create($input);

		$app->response()->write(json_encode($created));

	});


	##Get a form
	$app->get('/:uid', function($uid) use ($app) {
		$form = FormController::get_by_id($uid);

		$app->response()->write(json_encode($form));
	});
});

/* vim: set ts=4 sw=4 noet: */

